import React, { Component } from "react";

export class FetchData extends Component {
  static displayName = FetchData.name;

  constructor(props) {
    super(props);
    this.state = { forecasts: [], loading: true, title: "" };
  }

  componentDidMount() {
    this.populateWeatherData();
    this.populateTitle();
  }

  static renderForecastsTable(forecasts) {
    var image = "/images/" + forecasts.summary;
    return (
      <table className="table table-striped" aria-labelledby="tabelLabel">
        <thead>
          <tr>
            <th>Date</th>
            <th>Temp. (C)</th>
            <th>Temp. (F)</th>
            <th>Summary</th>
          </tr>
        </thead>
        <tbody>
          {forecasts.map(forecast => {
            var image =
              process.env.PUBLIC_URL + "/images/" + forecast.summary + ".jpg";
            return (
              <tr key={forecast.date}>
                <td>{forecast.date}</td>
                <td>{forecast.temperatureC}</td>
                <td>{forecast.temperatureF}</td>
                <td>{forecast.summary}</td>
                <td>
                  <img src={image} alt={forecast.summary} width="300px;" />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  render() {
    let contents = this.state.loading ? (
      <p>
        <em>Loading...</em>
      </p>
    ) : (
      FetchData.renderForecastsTable(this.state.forecasts)
    );

    return (
      <div>
        <h1 id="tabelLabel">{this.state.title}</h1>

        {contents}
      </div>
    );
  }

  async populateWeatherData() {
    const response = await fetch("http://localhost:8888/weatherforecast");
    const data = await response.json();
    this.setState({ forecasts: data, loading: false });
  }
  async populateTitle() {
    const response = await fetch("http://localhost:8888/title");
    const title = await response.text();
    this.setState({ title: title });
  }
}
