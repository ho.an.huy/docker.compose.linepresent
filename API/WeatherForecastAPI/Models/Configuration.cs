using System.ComponentModel.DataAnnotations;

namespace WeatherForecastAPI.Models
{
    public class Configuration
    {
        [Key]
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
