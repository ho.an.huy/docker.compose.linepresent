using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using WeatherForecastAPI.Data;

namespace WeatherForecastAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TitleController : ControllerBase
    {
        private AppDbContext _context;
        private readonly ILogger<TitleController> _logger;
        public TitleController(AppDbContext context, ILogger<TitleController> logger)
        {
            _context = context;
            _logger = logger;
        }
        [HttpGet]
        public string Get()
        {
            try
            {
                var title = _context.Configurations.FirstOrDefault(c => c.Key == "Title")?.ToString(); ;
                _logger.LogInformation(title);
                return title;
            }
            catch (System.Exception e)
            {
                _logger.LogError(e, "Loi ne");
                throw;
            }
            
            
        }
            
    }
}
